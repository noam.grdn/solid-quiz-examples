package Question1;

public class OfekCipher implements Cipher, CipherDbHandler {

    private final InMemoryDB ciphersDB = new InMemoryDB();
    private final int gapSize;
    private final int numberOfRotations;
    LetterRotator letterRotator = new LetterRotator();

    public OfekCipher(int numberOfRotations, int gapSize) {
        this.numberOfRotations = numberOfRotations;
        this.gapSize = gapSize;
    }

    @Override
    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }

    @Override
    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }

    @Override
    public String generateCipherResult(String textToCipher, int numberOfRotations) {
        StringBuilder cipherResult = new StringBuilder();
        for (int i = 0; i < textToCipher.length(); i++) {
            char letter = generateCipherLetter(textToCipher.charAt(i), i%gapSize, numberOfRotations);
            cipherResult.append(letter);
        }
        return cipherResult.toString();
    }

    @Override
    public char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations) {
        if(replaceLetter > 0)
            return letterToCipher;
        else
            return letterRotator.rotateLetter(letterToCipher, numberOfRotations);
    }

    @Override
    public void updateCipher(String text) {
        this.ciphersDB.update(text);
    }

    @Override
    public void loadCipher(String text) {
        this.ciphersDB.load(text);
    }

    @Override
    public void deleteCipher(int textId) {
        this.ciphersDB.delete(textId);

    }

}
